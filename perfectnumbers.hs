divisors :: Integer -> [Integer]
divisors n = [x | x <- [1..n `div` 2], n `mod` x == 0]

perfectNumber :: Integer -> Bool
perfectNumber n = sum (divisors) == n

perfectNumbers = filter perfectNumber [1..]

-- Take first 4 perfect numbers
take 4 perfectNumbers
